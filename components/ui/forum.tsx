import Image from "next/image"
import Link from "next/link"

export const Forum = () => {
    return (
        <>
            <div className="bg-black h-[100vh] w-full flex justify-center items-center">
                <div className="flex justify-center item">
                    <div className="w-[80vw] max-w-[500px] flex flex-col items-stretch bg-black h-[400px] justify-around">
                        <div className="flex flex-col justify-center">
                            <label className="text-white text-center z-20 uppercase">Society Name: </label>
                            <input className="rounded text-black text-center z-20" type="text"></input>
                        </div>
                        <div className="flex flex-col justify-center">
                            <label className="text-white text-center z-20 uppercase">Email: </label>
                            <input className="rounded text-black text-center z-20" type="text"></input>
                        </div>
                        <div className="flex flex-col justify-center ">
                            <label className="text-white text-center z-20 uppercase">Type of ticket: </label>
                            <select className="rounded text-black text-center z-20">
                                <option value="">Simple</option>
                                <option value="">Assigned seat tickets</option>
                                <option value="">Coming soon...</option>
                            </select>
                        </div>
                        <div className="flex flex-col justify-center ">
                            <label className="text-white text-center z-20 uppercase">Number of tickets: </label>
                            <input className="rounded text-black text-center z-20" min="0" type="number"></input>
                        </div>
                        <div className="flex flex-col justify-center">
                            <label className="text-white text-center z-20 uppercase">Further description of the event: </label>
                            <input className="rounded text-black text-center z-20" type="text"></input>
                        </div>
                        <div className="flex flex-col justify-center ">
                            <label className="text-white text-center z-20 uppercase">Date of the event: </label>
                            <input className="rounded text-black text-center mx-auto px-4 z-20" min="0" type="date"></input>
                        </div>
                        <button className="text-white border-2 mx-auto px-4 rounded hover:bg-white hover:text-black transition duration-700 text-xl w-[100px]">Submit</button>
                    </div>
                </div>
            </div>
        </>
    )
}