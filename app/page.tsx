'use client';
import Image from 'next/image'
import { Navbar } from '@/components/ui/navbar';
import {Forum} from '@/components/ui/forum'
import Link from 'next/link'



export default function Home() {
  return (
    <>
      <Navbar />
      <div className='relative'>
        <div
        className="z-10 animate-starBackground"
        style={{
          height: '0.5px',
          width: '0.5px',
          top: '50%',
          left: '50%',
          backgroundColor: '#fffff',
          position: 'sticky',
          borderRadius: '50%',
          boxShadow:
            '21vw 22vh 1px 1px #ffffff ,13vw -49vh 1px 0px #ffffff ,-38vw -40vh 0px 0px #ffffff ,-18vw -23vh 0px 1px #ffffff ,-38vw -49vh 1px 0px #ffffff ,44vw 2vh 0px 1px #ffffff ,-35vw 6vh 0px 1px #ffffff ,-28vw 13vh 1px 0px #ffffff ,-37vw -23vh 0px 0px #ffffff ,-9vw 16vh 0px 0px #ffffff ,21vw -18vh 1px 0px #ffffff ,37vw 28vh 0px 0px #ffffff ,34vw -29vh 0px 1px #ffffff ,35vw 31vh 1px 0px #ffffff ,37vw -6vh 0px 0px #ffffff ,27vw -1vh 1px 1px #ffffff ,-11vw 28vh 0px 1px #ffffff ,-36vw 12vh 0px 1px #ffffff ,4vw 32vh 1px 0px #ffffff ,38vw -38vh 1px 0px #ffffff ,14vw -5vh 1px 0px #ffffff ,30vw 25vh 0px 1px #ffffff ,44vw 7vh 0px 1px #ffffff ,-50vw -23vh 0px 1px #ffffff ,0vw -14vh 0px 0px #ffffff ,50vw -34vh 1px 1px #ffffff ,48vw -46vh 1px 0px #ffffff ,41vw 32vh 1px 0px #ffffff ,23vw -20vh 0px 1px #ffffff ,23vw -13vh 1px 0px #ffffff ,-49vw -48vh 0px 1px #ffffff ,29vw -12vh 1px 1px #ffffff ,-33vw 50vh 0px 0px #ffffff ,30vw 14vh 1px 1px #ffffff ,20vw -26vh 0px 0px #ffffff ,-16vw -31vh 1px 0px #ffffff ,40vw 1vh 0px 1px #ffffff ,-28vw -43vh 0px 1px #ffffff ,13vw 48vh 1px 0px #ffffff ,49vw -43vh 0px 0px #ffffff ,12vw 45vh 0px 0px #ffffff ,-2vw -22vh 1px 0px #ffffff ,-2vw -41vh 1px 0px #ffffff ,6vw 13vh 1px 0px #ffffff ,-36vw 17vh 1px 1px #ffffff ,29vw -41vh 0px 1px #ffffff ,-32vw 15vh 1px 0px #ffffff ,19vw -44vh 0px 0px #ffffff ,-16vw 31vh 1px 0px #ffffff ,15vw 30vh 1px 0px #ffffff ,-34vw 35vh 0px 1px #ffffff ,13vw -1vh 1px 0px #ffffff ,-7vw -22vh 0px 1px #ffffff ,-20vw -35vh 0px 0px #ffffff ,13vw -37vh 0px 0px #ffffff ,-26vw 50vh 1px 1px #ffffff ,-25vw 40vh 1px 0px #ffffff ,45vw 8vh 0px 1px #ffffff ,-24vw 26vh 0px 0px #ffffff ,48vw 33vh 0px 0px #ffffff ,-17vw -26vh 0px 1px #ffffff ,46vw 36vh 0px 0px #ffffff ,46vw -35vh 0px 0px #ffffff ,-43vw -44vh 1px 0px #ffffff ,1vw -49vh 1px 0px #ffffff ,-37vw 12vh 0px 1px #ffffff ,-49vw -31vh 1px 0px #ffffff ,28vw 22vh 0px 0px #ffffff ,2vw -30vh 1px 0px #ffffff ,-3vw -24vh 0px 0px #ffffff ,-45vw -16vh 0px 1px #ffffff ,44vw -45vh 0px 1px #ffffff ,-23vw -33vh 0px 0px #ffffff ,-49vw 28vh 0px 1px #ffffff ,-30vw 5vh 0px 0px #ffffff ,31vw -5vh 1px 1px #ffffff ,-12vw -28vh 0px 0px #ffffff ,-43vw 49vh 0px 1px #ffffff ,49vw 20vh 0px 0px #ffffff ,-39vw 20vh 1px 1px #ffffff ,-20vw 42vh 1px 1px #ffffff ,-47vw -50vh 1px 1px #ffffff ,-23vw 44vh 0px 1px #ffffff ,34vw 21vh 1px 1px #ffffff ,-43vw -9vh 1px 0px #ffffff ,-28vw 12vh 0px 1px #ffffff ,27vw 38vh 1px 0px #ffffff ,-35vw -14vh 0px 1px #ffffff ,-21vw 1vh 0px 0px #ffffff ,-31vw 45vh 0px 0px #ffffff ,22vw -33vh 0px 0px #ffffff ,-6vw 23vh 0px 1px #ffffff ,8vw -36vh 0px 1px #ffffff ,0vw -43vh 1px 0px #ffffff ,-2vw 29vh 1px 1px #ffffff ,46vw -10vh 1px 0px #ffffff ,-24vw 47vh 0px 0px #ffffff ,-40vw 41vh 1px 0px #ffffff ,-20vw -32vh 1px 0px #ffffff ,33vw -26vh 1px 0px #ffffff '
        }}></div>
        <div className='w-full h-[100vh] flex items-center justify-center'>
          <Forum></Forum>
        </div>
      </div>
    </>
  )
}
